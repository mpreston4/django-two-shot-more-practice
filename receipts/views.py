from django.shortcuts import render, redirect, get_object_or_404
from receipts.models import Receipt, ExpenseCategory, Account


def home(request):
    receipts_list = Receipt.objects.all()
    context = {"receipts_list": receipts_list}
    return render(request, "receipts/list.html", context)
